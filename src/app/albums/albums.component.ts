import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Album } from '../models/album.model';
import { AlbumsService } from '../services/albums.service';

@Component({
    selector: 'app-albums',
    templateUrl: './albums.component.html',
    styleUrls: ['./albums.component.css'],
})
export class AlbumsComponent implements OnInit {
    albums: Album[] = null;

    constructor(private albumsService: AlbumsService, private router: Router) {}

    ngOnInit(): void {
        this.getAlbums();
    }

    getFormattedDate(date: string | undefined): string {
        if (!date) return 'Not defined';
        return moment(date).format('MMMM Do YYYY');
    }

    getAlbums(): void {
        this.albumsService.getAlbums().subscribe(
            albums => (this.albums = albums),
            error => console.log(`Error getting albums. ${error.message}`)
        );
    }

    navigateToEdit(albumId): void {
        this.router.navigateByUrl(`/albums/${albumId}`);
    }

    navigateToNew(): void {
        this.router.navigateByUrl(`/albums/new`);
    }

    deleteAlbum(albumId): void {
        this.albumsService.deleteAlbum(albumId).subscribe(
            albumResponse => {
                console.log(`Deleted succesfully with id ${albumResponse._id}`);
                this.albums = this.albums.filter(album => album._id !== albumResponse._id);
            },
            error => console.log(`Error deleting. ${error.message}`)
        );
    }
}
