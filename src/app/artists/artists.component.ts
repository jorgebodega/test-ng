import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { Artist } from '../models/artist.model';
import { ArtistsService } from '../services/artists.service';

@Component({
    selector: 'app-artists',
    templateUrl: './artists.component.html',
    styleUrls: ['./artists.component.css'],
})
export class ArtistsComponent implements OnInit {
    artists: Artist[] = null;

    constructor(private artistsService: ArtistsService, private router: Router) {}

    ngOnInit(): void {
        this.getArtists();
    }

    getFormattedDate(date: string | undefined): string {
        if (!date) return 'Not defined';
        return moment(date).format('MMMM Do YYYY');
    }

    getArtists(): void {
        this.artistsService.getArtists().subscribe(
            artists => (this.artists = artists),
            error => console.log(`Error getting artists. ${error.message}`)
        );
    }

    navigateToEdit(artistId): void {
        this.router.navigateByUrl(`/artists/${artistId}`);
    }

    navigateToNew(): void {
        this.router.navigateByUrl(`/artists/new`);
    }

    deleteArtist(artistId): void {
        this.artistsService.deleteArtist(artistId).subscribe(
            artistResponse => {
                console.log(`Deleted succesfully with id ${artistResponse._id}`);
                this.artists = this.artists.filter(artist => artist._id !== artistResponse._id);
            },
            error => console.log(`Error deleting. ${error.message}`)
        );
    }
}
