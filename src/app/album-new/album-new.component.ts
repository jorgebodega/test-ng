import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Album } from '../models/album.model';
import { AlbumService } from '../services/album.service';

@Component({
    selector: 'app-album-new',
    templateUrl: './album-new.component.html',
    styleUrls: ['./album-new.component.css'],
})
export class AlbumNewComponent implements OnInit {
    album: Album = {
        title: '',
    };

    constructor(private albumService: AlbumService, private router: Router) {}

    ngOnInit(): void {}

    createAlbum(): void {
        this.albumService.createAlbum(this.album).subscribe(
            album => {
                this.router.navigateByUrl(`albums/${album._id}`);
            },
            error => console.log(`Error creating artist. ${error.message}`)
        );
    }
}
