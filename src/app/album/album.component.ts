import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Album } from '../models/album.model';
import { AlbumService } from '../services/album.service';

@Component({
    selector: 'app-album',
    templateUrl: './album.component.html',
    styleUrls: ['./album.component.css'],
})
export class AlbumComponent implements OnInit {
    id: number;
    album: Album;

    constructor(private albumService: AlbumService, private route: ActivatedRoute) {}

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.id = params.id;
            this.getAlbum(params.id);
        });
    }

    getAlbum(idAlbum): void {
        this.albumService.getAlbum(idAlbum).subscribe(
            album => (this.album = album),
            error => console.log(`Error getting artist. ${error.message}`)
        );
    }

    editAlbum(idAlbum) {
        this.albumService.editAlbum(idAlbum, this.album).subscribe(
            album => 'Artist modified successfully',
            error => console.log(`Error getting artist. ${error.message}`)
        );
    }
}
