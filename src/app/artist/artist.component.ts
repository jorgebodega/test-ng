import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

import { Artist } from '../models/artist.model';
import { ArtistService } from '../services/artist.service';

@Component({
    selector: 'app-artist',
    templateUrl: './artist.component.html',
    styleUrls: ['./artist.component.css'],
})
export class ArtistComponent implements OnInit {
    id: number;
    artist: Artist;
    minDate: Date = new Date('1909-01-01T00:00:00.000Z');
    maxDate: Date = new Date('2030-12-31T00:00:00.000Z');

    constructor(private artistService: ArtistService, private route: ActivatedRoute) {}

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.id = params.id;
            this.getArtist(params.id);
        });
    }

    getFormattedDate(date: string | undefined): string {
        if (!date) return 'Not defined';
        return moment(date).format('MMMM Do YYYY');
    }

    getArtist(idArtist): void {
        this.artistService.getArtist(idArtist).subscribe(
            artist => (this.artist = artist),
            error => console.log(`Error getting artist. ${error.message}`)
        );
    }

    editArtist(idArtist) {
        this.artistService.editArtist(idArtist, this.artist).subscribe(
            artist => 'Artist modified successfully',
            error => console.log(`Error getting artist. ${error.message}`)
        );
    }
}
