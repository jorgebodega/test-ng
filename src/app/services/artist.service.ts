import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Artist } from '../models/artist.model';

@Injectable({
    providedIn: 'root',
})
export class ArtistService {
    private artistUrl = `${environment.apiUrl}/artist`;

    constructor(private httpClient: HttpClient) {}

    getArtist(idArtist: string): Observable<Artist> {
        return this.httpClient.get<Artist>(`${this.artistUrl}/${idArtist}`);
    }

    createArtist(artist: Artist): Observable<Artist> {
        return this.httpClient.post<Artist>(`${this.artistUrl}`, artist);
    }

    editArtist(idArtist: string, artist: Artist): Observable<Artist> {
        return this.httpClient.put<Artist>(`${this.artistUrl}/${idArtist}`, artist);
    }

    deleteArtist(idArtist: string): Observable<Artist> {
        return this.httpClient.delete<Artist>(`${this.artistUrl}/${idArtist}`);
    }
}
