import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Album } from '../models/album.model';

@Injectable({
    providedIn: 'root',
})
export class AlbumsService {
    private albumsUrl = `${environment.apiUrl}/albums`;
    private albumUrl = `${environment.apiUrl}/album`;

    constructor(private httpClient: HttpClient) {}

    getAlbums(): Observable<Album[]> {
        return this.httpClient.get<Album[]>(`${this.albumsUrl}/all`);
    }

    getAlbum(idAlbum): Observable<Album> {
        return this.httpClient.get<Album>(`${this.albumUrl}/${idAlbum}`);
    }

    createAlbum(artist: Album): Observable<Album> {
        return this.httpClient.post<Album>(`${this.albumUrl}`, artist);
    }

    bulkCreateAlbums(albums: Album[]): Observable<Album> {
        return this.httpClient.put<Album>(`${this.albumsUrl}`, albums);
    }

    editAlbums(idAlbum, album: Album): Observable<Album> {
        return this.httpClient.put<Album>(`${this.albumUrl}/${idAlbum}`, album);
    }

    deleteAlbum(idAlbum): Observable<Album> {
        return this.httpClient.delete<Album>(`${this.albumUrl}/${idAlbum}`);
    }
}
