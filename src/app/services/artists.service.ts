import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Artist } from '../models/artist.model';

@Injectable({
    providedIn: 'root',
})
export class ArtistsService {
    private artistsUrl = `${environment.apiUrl}/artists`;
    private artistUrl = `${environment.apiUrl}/artist`;

    constructor(private httpClient: HttpClient) {}

    getArtists(): Observable<Artist[]> {
        return this.httpClient.get<Artist[]>(`${this.artistsUrl}/all`);
    }

    getArtist(idArtist): Observable<Artist> {
        return this.httpClient.get<Artist>(`${this.artistUrl}/${idArtist}`);
    }

    createArtist(artist: Artist): Observable<Artist> {
        return this.httpClient.post<Artist>(`${this.artistUrl}`, artist);
    }

    bulkCreateArtists(artists: Artist[]): Observable<Artist> {
        return this.httpClient.put<Artist>(`${this.artistsUrl}`, artists);
    }

    editArtist(idArtist, artist: Artist): Observable<Artist> {
        return this.httpClient.put<Artist>(`${this.artistUrl}/${idArtist}`, artist);
    }

    deleteArtist(idArtist): Observable<Artist> {
        return this.httpClient.delete<Artist>(`${this.artistUrl}/${idArtist}`);
    }
}
