import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Album } from '../models/album.model';

@Injectable({
    providedIn: 'root',
})
export class AlbumService {
    private albumUrl = `${environment.apiUrl}/album`;

    constructor(private httpClient: HttpClient) {}

    getAlbum(idAlbum: string): Observable<Album> {
        return this.httpClient.get<Album>(`${this.albumUrl}/${idAlbum}`);
    }

    createAlbum(album: Album): Observable<Album> {
        return this.httpClient.post<Album>(`${this.albumUrl}`, album);
    }

    editAlbum(idAlbum: string, album: Album): Observable<Album> {
        return this.httpClient.put<Album>(`${this.albumUrl}/${idAlbum}`, album);
    }

    deleteAlbum(idAlbum: string): Observable<Album> {
        return this.httpClient.delete<Album>(`${this.albumUrl}/${idAlbum}`);
    }
}
