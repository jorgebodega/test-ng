import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ArtistService } from '../services/artist.service';
import { Artist } from '../models/artist.model';

@Component({
    selector: 'app-artist-new',
    templateUrl: './artist-new.component.html',
    styleUrls: ['./artist-new.component.css'],
})
export class ArtistNewComponent implements OnInit {
    artist: Artist = {
        name: '',
    };
    minDate: Date = new Date('1909-01-01T00:00:00.000Z');
    maxDate: Date = new Date('2030-12-31T00:00:00.000Z');

    constructor(private artistService: ArtistService, private router: Router) {}

    ngOnInit(): void {}

    getFormattedDate(date: string | undefined): string {
        if (!date) return 'Not defined';
        return moment(date).format('MMMM Do YYYY');
    }

    createArtist(): void {
        this.artistService.createArtist(this.artist).subscribe(
            artist => {
                this.router.navigateByUrl(`artists/${artist._id}`);
            },
            error => console.log(`Error creating artist. ${error.message}`)
        );
    }
}
