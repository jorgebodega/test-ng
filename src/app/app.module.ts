import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // <-- NgModel lives here
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ArtistsComponent } from './artists/artists.component';
import { ArtistComponent } from './artist/artist.component';
import { ArtistNewComponent } from './artist-new/artist-new.component';
import { AlbumComponent } from './album/album.component';
import { AlbumNewComponent } from './album-new/album-new.component';
import { AlbumsComponent } from './albums/albums.component';

@NgModule({
    declarations: [
        AppComponent,
        ArtistsComponent,
        ArtistComponent,
        ArtistNewComponent,
        AlbumComponent,
        AlbumNewComponent,
        AlbumsComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        MatGridListModule,
        MatNativeDateModule,
        MatCardModule,
        MatDatepickerModule,
        MatIconModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [MatNativeDateModule, MatDatepickerModule],
    bootstrap: [AppComponent],
})
export class AppModule {}
