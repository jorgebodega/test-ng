export interface Album {
    _id?: string;
    title: string;
    artistId?: string;
    coverUrl?: string;
    year?: Number;
    genre?: String;
    _createdAt?: string;
    _updatedAt?: string;
}
