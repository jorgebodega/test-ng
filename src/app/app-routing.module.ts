import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArtistsComponent } from './artists/artists.component';
import { ArtistComponent } from './artist/artist.component';
import { ArtistNewComponent } from './artist-new/artist-new.component';
import { AlbumsComponent } from './albums/albums.component';
import { AlbumNewComponent } from './album-new/album-new.component';
import { AlbumComponent } from './album/album.component';

const routes: Routes = [
    { path: 'artists', component: ArtistsComponent },
    { path: 'artists/new', component: ArtistNewComponent },
    { path: 'artists/:id', component: ArtistComponent },
    { path: 'albums', component: AlbumsComponent },
    { path: 'albums/new', component: AlbumNewComponent },
    { path: 'albums/:id', component: AlbumComponent },
    { path: '**', redirectTo: 'original' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
